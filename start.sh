CONTAINER_NAME="chrome-ssh-pa"
IMAGE_NAME="chrome-ssh-pa"
VIDEO_DEVICE="/dev/video0"
#VIDEO_GROUP_ID=82
SSHD_MAP_PORT=2222
DOCKER="sudo docker "

PARAMS="-d --name $CONTAINER_NAME --device=$VIDEO_DEVICE -p $SSHD_MAP_PORT:22"

[ ! -z $VIDEO_GROUP_ID ] && PARAMS="$PARAMS -e VIDEO_GROUP_ID=$VIDEO_GROUP_ID "

$DOCKER run $PARAMS $IMAGE_NAME

#Remove old host id entries in known_host file.
ssh-keygen -R [127.0.0.1]:$SSHD_MAP_PORT
#For some reason, if we try to ssh to the container right after the host id 
#is removed from known_hosts, the sshd daemon closes the connection.
sleep 1

#ssh into the container and launch chrome
ssh docker-chrome /chrome-pa

#remove the container at the end
sudo docker rm -f chrome-ssh-pa