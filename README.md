# Google Chrome container with PulseAudio support

On the host machine PulseAudio needs to be enabled to allow *network access*. Launch *paprefs* and go to the *"Network Server"* tab, check the *"Enable network access to local sound devices"* and *"Don't require authentication"* checkboxes. PulseAudio needs to be restarted after this.

## How to build the container

1. Clone the repository:

        git clone git@bitbucket.org:jbaptiste/docker-chrome.git 
    
    or:
    
        git clone https://username@bitbucket.org/jbaptiste/docker-chrome.git
        
    Replace *username* for your bitbucket username on HTTPS.    

1. Build the container

        sudo docker build -t google-chrome .

The project includes a ssh key pair that can be used for *testing purposes*, but should be changed for new ones on production. The private key must go on the *$HOME/.ssh* directory of the user running the container.

## How to run the container

1. Copy
1. Create an entry in your .ssh/config file for easy access:
        
        Host docker-chrome
          User      chrome
          Port      2222
          HostName  127.0.0.1
          RemoteForward 64713 localhost:4713
          ForwardX11 yes

1. Run the container and start Google chrome using the [*start.sh*](https://bitbucket.org/jbaptiste/docker-chrome/src/67f3167d88083ff696d1c6ec8992aae4dcddfa41/start.sh?at=master) script:

        ./start.sh

   This script will do the following tasks:

   1. Launch the container with the needed parameters
   1. Remove the host id in the *known_hosts* file
   1. Run chrome
   1. When chrome is closed, delete the container. 

If using the demo keys don't forget to copy the private key (*id_rsa* to your *$HOME/.ssh* directory).