FROM ubuntu:latest
MAINTAINER  Juan Luis Baptiste <juan.baptiste@gmail.com>

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y ca-certificates \
    openssh-server pulseaudio wget
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb -P /tmp/
RUN dpkg -i /tmp/google-chrome-stable_current_amd64.deb || true
RUN apt-get install -fy
RUN adduser --disabled-password --gecos "Chrome User" chrome && usermod -G video chrome
RUN mkdir /var/run/sshd && mkdir /home/chrome/.ssh
COPY files/id_rsa.pub /home/chrome/.ssh/authorized_keys
RUN chown -R chrome:chrome /home/chrome/.ssh
COPY files/chrome-pa /
COPY files/run.sh /
RUN chmod 755 /chrome-pa && chmod 755 /run.sh

ENTRYPOINT ["/run.sh"]
# Expose the SSH port
EXPOSE 22
